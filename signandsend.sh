set -e

repo=flatpak-kde-applications
dest=flatpak@racnoss.kde.org:/srv/www/distribute.kde.org/flatpak-apps-testing/

echo "signing $repo"
flatpak build-commit-from --src-repo=$repo/repo --gpg-sign=61C45BED $repo/public-repo --update-appstream

echo "signing arm"
flatpak build-commit-from --src-repo=repo-arm --gpg-sign=61C45BED $repo/public-repo --update-appstream

echo "testing for appstream in $repo"
ostree --repo=$repo/public-repo refs | grep appstream

echo "sending signed $repo to $dest"
rsync -a $repo/public-repo/ $dest

scp $repo/*.flatpakrepo $dest
scp ~/logs/log-$repo.text $dest


