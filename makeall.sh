set -e
set -o pipefail

starttime=`date +%Y-%m-%d.%H:%M:%S`

function ourmake
{
    dir=$1
    shift
    cd ~/$dir
    rm -rf .flatpak-builder/build/
    git pull
    make $@ all export 2>&1 | tee ~/logs/log-$dir.text
    echo "make finished: $?" > ~/logs/log-$dir.text
}

export ARGS=--user
if [ "$HOSTNAME" = "joy" ]; then
    export EXPORT_ARGS="--gpg-sign=0D055622"
elif [ "$HOSTNAME" = "test2.arm32.com.local.lan" ]; then
    export EXPORT_ARGS="--gpg-sign=4D669FB6"
else
    echo "don't know which key to use"
fi

flatpak update -y $ARGS

INSTALL_SOURCE= ourmake flatpak-kde-applications -k
